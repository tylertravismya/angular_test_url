import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ResponseOptionService } from './ResponseOption.service';

describe('ResponseOptionService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [ResponseOptionService] });
	});

  it('should be created', () => {
    const service: ResponseOptionService = TestBed.get(ResponseOptionService);
    expect(service).toBeTruthy();
  });
});
