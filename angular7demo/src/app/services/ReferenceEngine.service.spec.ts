import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ReferenceEngineService } from './ReferenceEngine.service';

describe('ReferenceEngineService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [ReferenceEngineService] });
	});

  it('should be created', () => {
    const service: ReferenceEngineService = TestBed.get(ReferenceEngineService);
    expect(service).toBeTruthy();
  });
});
