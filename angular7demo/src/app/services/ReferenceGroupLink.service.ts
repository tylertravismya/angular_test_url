import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {ReferenceGroupLink} from '../models/ReferenceGroupLink';
import {ReferrerGroupService} from '../services/ReferrerGroup.service';
import {UserService} from '../services/User.service';
import { BaseService } from './base.service';

@Injectable()
export class ReferenceGroupLinkService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	referenceGroupLink : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a ReferenceGroupLink 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addReferenceGroupLink(dateLinkCreated, name, ReferrerGroup, LinkProvider) : Promise<any> {
    	const uri = this.ormUrl + '/ReferenceGroupLink/add';
    	const obj = {
      		dateLinkCreated: dateLinkCreated,
      		name: name,
      		ReferrerGroup: ReferrerGroup != null && ReferrerGroup.length > 0 ? ReferrerGroup : null,
			LinkProvider: LinkProvider != null && LinkProvider.length > 0 ? LinkProvider : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all ReferenceGroupLink 
	// returns the results untouched as JSON representation of an
	// array of ReferenceGroupLink models
	// delegates via URI to an ORM handler
	//********************************************************************
	getReferenceGroupLinks() {
    	const uri = this.ormUrl + '/ReferenceGroupLink';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a ReferenceGroupLink 
	// returns the results untouched as a JSON representation of a
	// ReferenceGroupLink model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editReferenceGroupLink(id) {
    	const uri = this.ormUrl + '/ReferenceGroupLink/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a ReferenceGroupLink 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateReferenceGroupLink(dateLinkCreated, name, ReferrerGroup, LinkProvider, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/ReferenceGroupLink/update/' + id;
    	const obj = {
      		dateLinkCreated: dateLinkCreated,
      		name: name,
      		ReferrerGroup: ReferrerGroup != null && ReferrerGroup.length > 0 ? ReferrerGroup : null,
			LinkProvider: LinkProvider != null && LinkProvider.length > 0 ? LinkProvider : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a ReferenceGroupLink 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteReferenceGroupLink(id)  : Promise<any> {
    	const uri = this.ormUrl + '/ReferenceGroupLink/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a ReferrerGroup on a ReferenceGroupLink
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignReferrerGroup( referenceGroupLinkId, referrerGroupId ): Promise<any> {

		// get the ReferenceGroupLink from storage
		this.loadHelper( referenceGroupLinkId );
		
		// get the ReferrerGroup from storage
		var referrerGroup 	= new ReferrerGroupService(this.http).editReferrerGroup(referrerGroupId);
		
		// assign the ReferrerGroup		
		this.referenceGroupLink.referrerGroup = referrerGroup;
      		
		// save the ReferenceGroupLink
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a ReferrerGroup on a ReferenceGroupLink
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignReferrerGroup( referenceGroupLinkId ): Promise<any> {

		// get the ReferenceGroupLink from storage
        this.loadHelper( referenceGroupLinkId );
		
		// assign ReferrerGroup to null		
		this.referenceGroupLink.referrerGroup = null;
      		
		// save the ReferenceGroupLink
		return this.saveHelper();
	}
	
	//********************************************************************
	// assigns a LinkProvider on a ReferenceGroupLink
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignLinkProvider( referenceGroupLinkId, linkProviderId ): Promise<any> {

		// get the ReferenceGroupLink from storage
		this.loadHelper( referenceGroupLinkId );
		
		// get the User from storage
		var user 	= new UserService(this.http).editUser(linkProviderId);
		
		// assign the LinkProvider		
		this.referenceGroupLink.linkProvider = user;
      		
		// save the ReferenceGroupLink
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a LinkProvider on a ReferenceGroupLink
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignLinkProvider( referenceGroupLinkId ): Promise<any> {

		// get the ReferenceGroupLink from storage
        this.loadHelper( referenceGroupLinkId );
		
		// assign LinkProvider to null		
		this.referenceGroupLink.linkProvider = null;
      		
		// save the ReferenceGroupLink
		return this.saveHelper();
	}
	


	//********************************************************************
	// saveHelper - internal helper to save a ReferenceGroupLink
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/ReferenceGroupLink/update/' + this.referenceGroupLink._id;		
		
    	return this
      			.http
      			.post(uri, this.referenceGroupLink)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a ReferenceGroupLink
	//********************************************************************	
	loadHelper( id ) {
		this.editReferenceGroupLink(id)
        		.subscribe(res => {
        			this.referenceGroupLink = res;
      			});
	}
}