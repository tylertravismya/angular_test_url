import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Comment} from '../models/Comment';
import {ReferrerService} from '../services/Referrer.service';
import { BaseService } from './base.service';

@Injectable()
export class CommentService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	comment : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Comment 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addComment(commentText, Source) : Promise<any> {
    	const uri = this.ormUrl + '/Comment/add';
    	const obj = {
      		commentText: commentText,
			Source: Source != null && Source.length > 0 ? Source : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Comment 
	// returns the results untouched as JSON representation of an
	// array of Comment models
	// delegates via URI to an ORM handler
	//********************************************************************
	getComments() {
    	const uri = this.ormUrl + '/Comment';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Comment 
	// returns the results untouched as a JSON representation of a
	// Comment model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editComment(id) {
    	const uri = this.ormUrl + '/Comment/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Comment 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateComment(commentText, Source, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Comment/update/' + id;
    	const obj = {
      		commentText: commentText,
			Source: Source != null && Source.length > 0 ? Source : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Comment 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteComment(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Comment/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a Source on a Comment
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignSource( commentId, sourceId ): Promise<any> {

		// get the Comment from storage
		this.loadHelper( commentId );
		
		// get the Referrer from storage
		var referrer 	= new ReferrerService(this.http).editReferrer(sourceId);
		
		// assign the Source		
		this.comment.source = referrer;
      		
		// save the Comment
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Source on a Comment
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignSource( commentId ): Promise<any> {

		// get the Comment from storage
        this.loadHelper( commentId );
		
		// assign Source to null		
		this.comment.source = null;
      		
		// save the Comment
		return this.saveHelper();
	}
	


	//********************************************************************
	// saveHelper - internal helper to save a Comment
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Comment/update/' + this.comment._id;		
		
    	return this
      			.http
      			.post(uri, this.comment)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Comment
	//********************************************************************	
	loadHelper( id ) {
		this.editComment(id)
        		.subscribe(res => {
        			this.comment = res;
      			});
	}
}