import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {QuestionGroup} from '../models/QuestionGroup';
import {QuestionService} from '../services/Question.service';
import { BaseService } from './base.service';

@Injectable()
export class QuestionGroupService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	questionGroup : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a QuestionGroup 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addQuestionGroup(aggregateScore, weight, name, Questions, QuestionGroups) : Promise<any> {
    	const uri = this.ormUrl + '/QuestionGroup/add';
    	const obj = {
      		aggregateScore: aggregateScore,
      		weight: weight,
      		name: name,
      		Questions: Questions != null && Questions.length > 0 ? Questions : null,
			QuestionGroups: QuestionGroups != null && QuestionGroups.length > 0 ? QuestionGroups : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all QuestionGroup 
	// returns the results untouched as JSON representation of an
	// array of QuestionGroup models
	// delegates via URI to an ORM handler
	//********************************************************************
	getQuestionGroups() {
    	const uri = this.ormUrl + '/QuestionGroup';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a QuestionGroup 
	// returns the results untouched as a JSON representation of a
	// QuestionGroup model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editQuestionGroup(id) {
    	const uri = this.ormUrl + '/QuestionGroup/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a QuestionGroup 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateQuestionGroup(aggregateScore, weight, name, Questions, QuestionGroups, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/QuestionGroup/update/' + id;
    	const obj = {
      		aggregateScore: aggregateScore,
      		weight: weight,
      		name: name,
      		Questions: Questions != null && Questions.length > 0 ? Questions : null,
			QuestionGroups: QuestionGroups != null && QuestionGroups.length > 0 ? QuestionGroups : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a QuestionGroup 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteQuestionGroup(id)  : Promise<any> {
    	const uri = this.ormUrl + '/QuestionGroup/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	
	//********************************************************************
	// adds one or more questionsIds as a Questions 
	// to a QuestionGroup
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addQuestions( questionGroupId, questionsIds ): Promise<any> {

		// get the QuestionGroup
		this.loadHelper( questionGroupId );
				
		// split on a comma with no spaces
		var idList = questionsIds.split(',')

		// iterate over array of questions ids
		idList.forEach(function (id) {
			// read the Question		
			var question = new QuestionService(this.http).editQuestion(id);	
			// add the Question if not already assigned
			if ( this.questionGroup.questions.indexOf(question) == -1 )
				this.questionGroup.questions.push(question);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more questionsIds as a Questions 
	// from a QuestionGroup
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeQuestions( questionGroupId, questionsIds ): Promise<any> {
		
		// get the QuestionGroup
		this.loadHelper( questionGroupId );

				
		// split on a comma with no spaces
		var idList 					= questionsIds.split(',');
		var questions 	= this.questionGroup.questions;
		
		if ( questions != null && questionsIds != null ) {
		
			// iterate over array of questions ids
			questions.forEach(function (obj) {				
				if ( questionsIds.indexOf(obj._id) > -1 ) {
					 // remove the Question
					this.questionGroup.questions.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			
	//********************************************************************
	// adds one or more questionGroupsIds as a QuestionGroups 
	// to a QuestionGroup
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addQuestionGroups( questionGroupId, questionGroupsIds ): Promise<any> {

		// get the QuestionGroup
		this.loadHelper( questionGroupId );
				
		// split on a comma with no spaces
		var idList = questionGroupsIds.split(',')

		// iterate over array of questionGroups ids
		idList.forEach(function (id) {
			// read the QuestionGroup		
			var questionGroup = new QuestionGroupService(this.http).editQuestionGroup(id);	
			// add the QuestionGroup if not already assigned
			if ( this.questionGroup.questionGroups.indexOf(questionGroup) == -1 )
				this.questionGroup.questionGroups.push(questionGroup);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more questionGroupsIds as a QuestionGroups 
	// from a QuestionGroup
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeQuestionGroups( questionGroupId, questionGroupsIds ): Promise<any> {
		
		// get the QuestionGroup
		this.loadHelper( questionGroupId );

				
		// split on a comma with no spaces
		var idList 					= questionGroupsIds.split(',');
		var questionGroups 	= this.questionGroup.questionGroups;
		
		if ( questionGroups != null && questionGroupsIds != null ) {
		
			// iterate over array of questionGroups ids
			questionGroups.forEach(function (obj) {				
				if ( questionGroupsIds.indexOf(obj._id) > -1 ) {
					 // remove the QuestionGroup
					this.questionGroup.questionGroups.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a QuestionGroup
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/QuestionGroup/update/' + this.questionGroup._id;		
		
    	return this
      			.http
      			.post(uri, this.questionGroup)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a QuestionGroup
	//********************************************************************	
	loadHelper( id ) {
		this.editQuestionGroup(id)
        		.subscribe(res => {
        			this.questionGroup = res;
      			});
	}
}