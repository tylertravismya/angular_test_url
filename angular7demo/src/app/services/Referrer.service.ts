import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Referrer} from '../models/Referrer';
import {CommentService} from '../services/Comment.service';
import { BaseService } from './base.service';

@Injectable()
export class ReferrerService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	referrer : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Referrer 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addReferrer(firstName, lastName, emailAddress, active, Comments) : Promise<any> {
    	const uri = this.ormUrl + '/Referrer/add';
    	const obj = {
      		firstName: firstName,
      		lastName: lastName,
      		emailAddress: emailAddress,
      		active: active,
			Comments: Comments != null && Comments.length > 0 ? Comments : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Referrer 
	// returns the results untouched as JSON representation of an
	// array of Referrer models
	// delegates via URI to an ORM handler
	//********************************************************************
	getReferrers() {
    	const uri = this.ormUrl + '/Referrer';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Referrer 
	// returns the results untouched as a JSON representation of a
	// Referrer model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editReferrer(id) {
    	const uri = this.ormUrl + '/Referrer/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Referrer 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateReferrer(firstName, lastName, emailAddress, active, Comments, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Referrer/update/' + id;
    	const obj = {
      		firstName: firstName,
      		lastName: lastName,
      		emailAddress: emailAddress,
      		active: active,
			Comments: Comments != null && Comments.length > 0 ? Comments : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Referrer 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteReferrer(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Referrer/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	
	//********************************************************************
	// adds one or more commentsIds as a Comments 
	// to a Referrer
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addComments( referrerId, commentsIds ): Promise<any> {

		// get the Referrer
		this.loadHelper( referrerId );
				
		// split on a comma with no spaces
		var idList = commentsIds.split(',')

		// iterate over array of comments ids
		idList.forEach(function (id) {
			// read the Comment		
			var comment = new CommentService(this.http).editComment(id);	
			// add the Comment if not already assigned
			if ( this.referrer.comments.indexOf(comment) == -1 )
				this.referrer.comments.push(comment);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more commentsIds as a Comments 
	// from a Referrer
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeComments( referrerId, commentsIds ): Promise<any> {
		
		// get the Referrer
		this.loadHelper( referrerId );

				
		// split on a comma with no spaces
		var idList 					= commentsIds.split(',');
		var comments 	= this.referrer.comments;
		
		if ( comments != null && commentsIds != null ) {
		
			// iterate over array of comments ids
			comments.forEach(function (obj) {				
				if ( commentsIds.indexOf(obj._id) > -1 ) {
					 // remove the Comment
					this.referrer.comments.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a Referrer
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Referrer/update/' + this.referrer._id;		
		
    	return this
      			.http
      			.post(uri, this.referrer)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Referrer
	//********************************************************************	
	loadHelper( id ) {
		this.editReferrer(id)
        		.subscribe(res => {
        			this.referrer = res;
      			});
	}
}