var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Comment
var Comment = new Schema({
  commentText: {
	type : String
  },
  Source: {
	type : Schema.Types.ObjectId
  },
},{
    collection: 'comments'
});

module.exports = mongoose.model('Comment', Comment);