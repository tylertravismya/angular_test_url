var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Activity
var Activity = new Schema({
  refObjId: {
	type : String
  },
  createDateTime: {
	type : String
  },
  Type: {
 	type : String
  },
  User: {
	type : Schema.Types.ObjectId
  },
},{
    collection: 'activitys'
});

module.exports = mongoose.model('Activity', Activity);