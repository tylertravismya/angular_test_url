
// enum type ${$enumName}
export let ProfessionType = {
	academic:"academic",
	technical:"technical",
	medical:"medical",
	construction:"construction",
	politics:"politics",
	professional:"professional",
	layworker:"layworker",
	legal:"legal",
	social:"social",
}

// enum type ${$enumName}
export let ReferenceStatus = {
	notYetStarted:"notYetStarted",
	inProgress:"inProgress",
	completed:"completed",
}

// enum type ${$enumName}
export let Purpose = {
	character:"character",
	business:"business",
	employment:"employment",
	residency:"residency",
}

// enum type ${$enumName}
export let ActivityType = {
	referenceStarted:"referenceStarted",
	referenceNotificationExpired:"referenceNotificationExpired",
	referenceCompleted:"referenceCompleted",
	referenceGroupChecked:"referenceGroupChecked",
	commentCreated:"commentCreated",
	referenceMadePublic:"referenceMadePublic",
	referenceMadePrivate:"referenceMadePrivate",
}
