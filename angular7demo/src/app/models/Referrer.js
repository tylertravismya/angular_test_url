var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Referrer
var Referrer = new Schema({
  firstName: {
	type : String
  },
  lastName: {
	type : String
  },
  emailAddress: {
	type : String
  },
  active: {
	type : Boolean
  },
  Comments: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Comment' }]
  },
},{
    collection: 'referrers'
});

module.exports = mongoose.model('Referrer', Referrer);