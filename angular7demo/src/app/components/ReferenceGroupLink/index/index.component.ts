import { ReferenceGroupLinkService } from '../../../services/ReferenceGroupLink.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ReferenceGroupLink } from '../../../models/ReferenceGroupLink';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexReferenceGroupLinkComponent implements OnInit {

  referenceGroupLinks: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferenceGroupLinkService) {}

  ngOnInit() {
    this.getReferenceGroupLinks();
  }

  getReferenceGroupLinks() {
    this.service.getReferenceGroupLinks().subscribe(res => {
      this.referenceGroupLinks = res;
    });
  }

  deleteReferenceGroupLink(id) {
    this.service.deleteReferenceGroupLink(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexReferenceGroupLink']));
			});  }
}
