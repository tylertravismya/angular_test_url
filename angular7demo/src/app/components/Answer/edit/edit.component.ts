import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnswerService } from '../../../services/Answer.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Answer/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditAnswerComponent extends SubBaseComponent implements OnInit {

  answer: any;
  answerForm: FormGroup;
  title = 'Edit Answer';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: AnswerService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.answerForm = this.fb.group({
      Question: ['', ],
      Response: ['', ]
   });
  }
  updateAnswer(Question, Response) {
    this.route.params.subscribe(params => {
    	this.service.updateAnswer(Question, Response, params['id'])
      		.then(success => this.router.navigate(['/indexAnswer']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.answer = this.service.editAnswer(params['id']).subscribe(res => {
        this.answer = res;
      });
    });
    
    super.ngOnInit();
  }
}
