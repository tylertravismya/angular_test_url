import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReferrerService } from '../../../services/Referrer.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Referrer/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditReferrerComponent extends SubBaseComponent implements OnInit {

  referrer: any;
  referrerForm: FormGroup;
  title = 'Edit Referrer';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferrerService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.referrerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      emailAddress: ['', Validators.required],
      active: ['', Validators.required],
      Comments: ['', ]
   });
  }
  updateReferrer(firstName, lastName, emailAddress, active, Comments) {
    this.route.params.subscribe(params => {
    	this.service.updateReferrer(firstName, lastName, emailAddress, active, Comments, params['id'])
      		.then(success => this.router.navigate(['/indexReferrer']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.referrer = this.service.editReferrer(params['id']).subscribe(res => {
        this.referrer = res;
      });
    });
    
    super.ngOnInit();
  }
}
