import { CommentService } from '../../../services/Comment.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Comment } from '../../../models/Comment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexCommentComponent implements OnInit {

  comments: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: CommentService) {}

  ngOnInit() {
    this.getComments();
  }

  getComments() {
    this.service.getComments().subscribe(res => {
      this.comments = res;
    });
  }

  deleteComment(id) {
    this.service.deleteComment(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexComment']));
			});  }
}
