import { ReferenceGiverService } from '../../../services/ReferenceGiver.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ReferenceGiver } from '../../../models/ReferenceGiver';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexReferenceGiverComponent implements OnInit {

  referenceGivers: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferenceGiverService) {}

  ngOnInit() {
    this.getReferenceGivers();
  }

  getReferenceGivers() {
    this.service.getReferenceGivers().subscribe(res => {
      this.referenceGivers = res;
    });
  }

  deleteReferenceGiver(id) {
    this.service.deleteReferenceGiver(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexReferenceGiver']));
			});  }
}
