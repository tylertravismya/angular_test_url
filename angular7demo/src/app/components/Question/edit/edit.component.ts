import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionService } from '../../../services/Question.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Question/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditQuestionComponent extends SubBaseComponent implements OnInit {

  question: any;
  questionForm: FormGroup;
  title = 'Edit Question';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: QuestionService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.questionForm = this.fb.group({
      weight: ['', Validators.required],
      questionText: ['', Validators.required],
      identifier: ['', Validators.required],
      mustBeAnswered: ['', Validators.required],
      responseExclusive: ['', Validators.required],
      Responses: ['', ]
   });
  }
  updateQuestion(weight, questionText, identifier, mustBeAnswered, responseExclusive, Responses) {
    this.route.params.subscribe(params => {
    	this.service.updateQuestion(weight, questionText, identifier, mustBeAnswered, responseExclusive, Responses, params['id'])
      		.then(success => this.router.navigate(['/indexQuestion']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.question = this.service.editQuestion(params['id']).subscribe(res => {
        this.question = res;
      });
    });
    
    super.ngOnInit();
  }
}
