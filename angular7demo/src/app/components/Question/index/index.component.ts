import { QuestionService } from '../../../services/Question.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Question } from '../../../models/Question';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexQuestionComponent implements OnInit {

  questions: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: QuestionService) {}

  ngOnInit() {
    this.getQuestions();
  }

  getQuestions() {
    this.service.getQuestions().subscribe(res => {
      this.questions = res;
    });
  }

  deleteQuestion(id) {
    this.service.deleteQuestion(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexQuestion']));
			});  }
}
