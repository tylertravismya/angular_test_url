// questionRoutes.js

var express = require('express');
var app = express();
var questionRoutes = express.Router();

// Require Item model in our routes module
var Question = require('../models/Question');

// Defined store route
questionRoutes.route('/add').post(function (req, res) {
	var question = new Question(req.body);
	question.save()
    .then(item => {
    	res.status(200).json({'question': 'Question added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
questionRoutes.route('/').get(function (req, res) {
	Question.find(function (err, questions){
		if(err){
			console.log(err);
		}
		else {
			res.json(questions);
		}
	});
});

// Defined edit route
questionRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Question.findById(id, function (err, question){
		res.json(question);
	});
});

//  Defined update route
questionRoutes.route('/update/:id').post(function (req, res) {
	Question.findById(req.params.id, function(err, question) {
		if (!question)
			return next(new Error('Could not load a Question Document using id ' + req.params.id));
		else {
            question.weight = req.body.weight;
            question.questionText = req.body.questionText;
            question.identifier = req.body.identifier;
            question.mustBeAnswered = req.body.mustBeAnswered;
            question.responseExclusive = req.body.responseExclusive;
            question.Responses = req.body.Responses;

			question.save().then(question => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
questionRoutes.route('/delete/:id').get(function (req, res) {
   Question.findOneAndDelete({_id: req.params.id}, function(err, question){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Question + ' using id ' + req.params.id );
    });
});

module.exports = questionRoutes;