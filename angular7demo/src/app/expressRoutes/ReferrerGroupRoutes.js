// referrerGroupRoutes.js

var express = require('express');
var app = express();
var referrerGroupRoutes = express.Router();

// Require Item model in our routes module
var ReferrerGroup = require('../models/ReferrerGroup');

// Defined store route
referrerGroupRoutes.route('/add').post(function (req, res) {
	var referrerGroup = new ReferrerGroup(req.body);
	referrerGroup.save()
    .then(item => {
    	res.status(200).json({'referrerGroup': 'ReferrerGroup added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
referrerGroupRoutes.route('/').get(function (req, res) {
	ReferrerGroup.find(function (err, referrerGroups){
		if(err){
			console.log(err);
		}
		else {
			res.json(referrerGroups);
		}
	});
});

// Defined edit route
referrerGroupRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	ReferrerGroup.findById(id, function (err, referrerGroup){
		res.json(referrerGroup);
	});
});

//  Defined update route
referrerGroupRoutes.route('/update/:id').post(function (req, res) {
	ReferrerGroup.findById(req.params.id, function(err, referrerGroup) {
		if (!referrerGroup)
			return next(new Error('Could not load a ReferrerGroup Document using id ' + req.params.id));
		else {
            referrerGroup.name = req.body.name;
            referrerGroup.dateTimeLastViewedExternally = req.body.dateTimeLastViewedExternally;
            referrerGroup.References = req.body.References;

			referrerGroup.save().then(referrerGroup => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
referrerGroupRoutes.route('/delete/:id').get(function (req, res) {
   ReferrerGroup.findOneAndDelete({_id: req.params.id}, function(err, referrerGroup){
        if(err) res.json(err);
        else res.json('Successfully removed ' + ReferrerGroup + ' using id ' + req.params.id );
    });
});

module.exports = referrerGroupRoutes;