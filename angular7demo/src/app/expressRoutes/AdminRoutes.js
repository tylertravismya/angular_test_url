// adminRoutes.js

var express = require('express');
var app = express();
var adminRoutes = express.Router();

// Require Item model in our routes module
var Admin = require('../models/Admin');

// Defined store route
adminRoutes.route('/add').post(function (req, res) {
	var admin = new Admin(req.body);
	admin.save()
    .then(item => {
    	res.status(200).json({'admin': 'Admin added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
adminRoutes.route('/').get(function (req, res) {
	Admin.find(function (err, admins){
		if(err){
			console.log(err);
		}
		else {
			res.json(admins);
		}
	});
});

// Defined edit route
adminRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Admin.findById(id, function (err, admin){
		res.json(admin);
	});
});

//  Defined update route
adminRoutes.route('/update/:id').post(function (req, res) {
	Admin.findById(req.params.id, function(err, admin) {
		if (!admin)
			return next(new Error('Could not load a Admin Document using id ' + req.params.id));
		else {
            admin.loginId = req.body.loginId;
            admin.password = req.body.password;
            admin.Users = req.body.Users;
            admin.ReferenceEngines = req.body.ReferenceEngines;

			admin.save().then(admin => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
adminRoutes.route('/delete/:id').get(function (req, res) {
   Admin.findOneAndDelete({_id: req.params.id}, function(err, admin){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Admin + ' using id ' + req.params.id );
    });
});

module.exports = adminRoutes;